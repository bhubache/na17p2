# L'objectif du projet est de réaliser une base de donnée permettant d'émettre des bulletins météo.

# Liste des objets qui devront être gérés:

- Bulletin
- Capteur
- Lieu
- Ville
- Département
- Région
- Massif-Montagnieux
- Date
- Prévision
- Temperature
- Precipitation
- Vent

# Liste des popriétés associées à chaque objet:

**Bulletin:** 
* Lieu
* Date
* Previsions : un minimum de 2 prévisions, toutes de type différent
* Periode : {matin, après-midi, soirée, nuit}
* Capteurs
* **Contraintes: Aucuns des attributs ci-dessus ne doit être null, (Lieu, Date, Periode) unique**

**Lieu:**
* Nom : unique
* Type : {Massif-Montagnieux, Ville}

**Ville:**
* Nom : unique
* Département : un seul département

**Massif-Montagnieux:**
* Nom : unique
* Départements : 1 ou 2 départements

**Département:**
* Nom : unique
* Région : une seule région

**Région:**
* Nom : unique

**Date:**
* Jour
* Mois
* Année
* Bulletins: entre 0 et 4 bulletins, tous d'une période différente
* **Contraintes : Jour, Mois et Année ne doivent pas être null, (Jour, Mois, Année) unique**

**Prévision:**
* Type : {Température, Précipitation, Vent}
* Description : optionnelle

**Température:**
* Réelle : entier (température exprimée en degré celsius), not null
* Ressentie : optionnelle

**Précipitation:**
* Type : {pluie, neige, grêle}

**Vent:**
* Vitesse : en km/h, not null
* Direction : {Nord, Sud, Est, Ouest}

**Capteur:**
* ID : unique
* Lieu : not null
* Historique

**Historique:**
* Lieux
* Début : Date
* Fin : Date
* **Contraintes : aucun des attributs ci-dessus ne doit être null**



